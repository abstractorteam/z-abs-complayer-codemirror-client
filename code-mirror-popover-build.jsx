
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class CodeMirrorPopoverBuild extends ReactComponentBase {
  constructor(props) {
    super(props);
  }

  shouldUpdate(nextProps, nextState) {
    return true;//!this.deepCompare(this.props.currentValue, nextProps.currentValue)
      //|| !this.deepCompare(this.props.objectValues, nextProps.objectValues)
  }
  
  renderTitle(id) {
    return (
      <div id={id}>
        <p id="debug_popover_title_title">
          Build Error
        </p>
        <button type="button" id="debug_popover_title_close" className="close" aria-label="Close"
          onClick={(e) => {
            this.props.onClose && this.props.onClose();
          }}
        >
          ×
        </button>
      </div>
    );
  }
  
  render() {
    return (
      <div id="debug_popover_tooltip">
        {this.renderTitle('debug_popover_title_value')}
        <div id="debug_popover_content">
          {this.props.message}
        </div>
      </div>
    );
  }
}
