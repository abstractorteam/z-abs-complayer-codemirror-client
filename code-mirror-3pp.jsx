

window.CodeMirror = require('codemirror');
require('codemirror/mode/javascript/javascript');
require('codemirror/addon/hint/show-hint');
require('codemirror/addon/hint/javascript-hint');
require('codemirror/mode/htmlmixed/htmlmixed');
require('codemirror/addon/hint/html-hint');
require('codemirror/mode/css/css');
require('codemirror/addon/hint/css-hint');
require('codemirror/mode/jsx/jsx');
require('cm-show-invisibles');

require('codemirror/addon/dialog/dialog');
require('codemirror/addon/search/search');
require('codemirror/addon/search/searchcursor');
require('codemirror/addon/search/jump-to-line');
require('codemirror/addon/selection/mark-selection');

