
'use strict';

import CodeMirrorPopover from './code-mirror-popover';
import CodeMirrorPopoverBuild from './code-mirror-popover-build';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';
import ReactDOM from 'react-dom';


export default class CodeMirrorEditor extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.code = this.props.code;
    this.focusTrigger = true;
    this.breakpoint = null;
    this.popoverDiv = null;
    this.reactDiv = null;
    this.currentTarget = null;
    this._textarea = null;
    this.showTooltip = 0;
  }
  
  didMount() {
    this.codeMirror = CodeMirror.fromTextArea(this._textarea, this.props.options);
    //this.codeMirror.autofocus = false;
    this.codeMirror.dragDrop = false;
    this.codeMirror.setValue(this.props.code);
    this.cleanId = this.codeMirror.changeGeneration();
    this.codeMirror.clearHistory();
    this.codeMirror.on('change', (docKey, change) => this.onCodeChange(docKey, change));
   // this.codeMirror.on('focus', () => this.onFocusChanged(true));
   // this.codeMirror.on('blur', () => this.onFocusChanged(false));

    if(undefined !== this.props.options.gutters && undefined !== this.props.dbugger) {
      this.codeMirror.on('gutterClick', (cm, lineNbr, g) => {
        const lineInfo = this.codeMirror.lineInfo(lineNbr);
        this.props.onGutterChange && this.props.onGutterChange(this.props.docKey, lineInfo.line, undefined !== lineInfo.gutterMarkers && null !== lineInfo.gutterMarkers);
      });
      this._showDebugGutters(this.props.dbugger, this.props.name);
    }
    if(undefined !== this.props.currentQuery) {
      if(undefined !== this.props.currentQuery.line) {
        const url = `${this.props.currentFile.path}/${this.props.currentFile.title}`
        if(url === this.props.currentQuery.url) {
          const line = this.props.currentQuery.line - 1;
          this.codeMirror.addLineClass(line, 'background', this.props.currentQuery.typeCss);
          this.codeMirror.scrollIntoView({line: line, ch: 0}, 100);
        }
      }
    }
    if(undefined !== this.props.height) {
      this.setHeight(this.props.height);
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return this.props !== nextProps;
  }
  
  didUpdate(prevProps, prevState) {
    if(prevProps.code !== this.props.code) {
      if(this.code !== this.props.code) {
        this.codeMirror.setValue(this.props.code);
        this.code = this.props.code;
      }
    }
    
    if('file' === this.props.currentType) {
      if(this.props.currentFile && this.props.docKey === this.props.currentFile.key) {
        this.focusTrigger = false;
        this.codeMirror.focus();
      }
      if(typeof this.props.options === 'object') {
        if(!this.shallowCompareObjectValues(prevProps.options, this.props.options)) {
          for(let optionName in this.props.options) {
            this.codeMirror.setOption(optionName, this.props.options[optionName]);
          }
        }
      }
    }
    
    // DEBUG STATE

    if(undefined !== this.props.options.gutters && prevProps.dbugger !== this.props.dbugger) {
      const previus = prevProps.docKey === prevProps.dbugger.actor.index;
      const next = this.props.docKey === this.props.dbugger.actor.index;
      if(previus && next) {
        // From 'debug_no_break' to 'debug_break'
        if(0 === prevProps.dbugger.state && 1 === this.props.dbugger.state) {
          if(this._showScript(this.props.dbugger)) {
            this._showCurrentBreakpoint(this.props.dbugger);
            this._showDebugGutters(this.props.dbugger, this.props.name);
          }
          this._bindPopover();
        }
        // From 'debug_break' to 'debug_no_break'
        if(1 === prevProps.dbugger.state && 0 === this.props.dbugger.state) {
          this._hideCurrentBreakpoint();
          this._unbindPopover();
        }
      }
      else if(previus) {
        // From 'debug_break' to 'debug_no_break'
        if(1 === prevProps.dbugger.state && 0 === this.props.dbugger.state) {
          this._hideCurrentBreakpoint();
          this._unbindPopover();
        }
      }
      else if(next) {
        // From 'debug_no_break' to 'debug_break'
        if(0 === prevProps.dbugger.state && 1 === this.props.dbugger.state) {
          if(this._showScript(this.props.dbugger)) {
            this._showCurrentBreakpoint(this.props.dbugger);
            this._showDebugGutters(this.props.dbugger, this.props.name);
          }
          this._bindPopover();
        }
      }
      
      if(prevProps.dbugger.breakpoints !== this.props.dbugger.breakpoints) {
        this._showDebugGutters(this.props.dbugger, this.props.name);
      }

      if((this.props.docKey === this.props.dbugger.actor.index) && ((prevProps.dbugger.level !== this.props.dbugger.level) || (prevProps.dbugger.scripts !== this.props.dbugger.scripts))) {
        if(this._showScript(this.props.dbugger)) {
          this._showCurrentBreakpoint(this.props.dbugger);
          this._showDebugGutters(this.props.dbugger, this.props.name);
        }
      }
      
      if(0 === prevProps.dbugger.state && 2 === this.props.dbugger.state) {
        this._showActorFile(this.props.dbugger);
      }
      
      this._showPopup(this.props.dbugger);
    }
    
    if(prevProps.build !== this.props.build) {
      this._showBuildGutters();
    }
  }
  
  willUnmount() {
    if(this.codeMirror) {
      this.codeMirror.toTextArea();
    }
  }

  setHeight(height) {
    this.codeMirror.setSize(null, height);
  }
  
  _showScript(dbugger) {
    const stackLevel = dbugger.stack[dbugger.level];
    const code = dbugger.scripts.get(stackLevel.location.scriptId);
    if(undefined !== code) {
      if(this.code !== code) {
        this.codeMirror.setValue(code);
        this.code = code;
      }
      return true;
    }
    else {
      return false;
    }
  }
  
  _showActorFile(dbugger) {
    const code = dbugger.actorFiles.get(this.props.docKey).content;
    if(this.code !== code) {
      this.codeMirror.setValue(code);
      this.code = code;
    }
  }

  _showCurrentBreakpoint(dbugger) {
    const breakpoint = {
      lineNumber: dbugger.stack[dbugger.level].location.lineNumber,
      className: 0 === dbugger.level ? 'debug_break_level_0' : 'debug_break_level_0_plus_x'
    };
    this._hideCurrentBreakpoint();
    this.codeMirror.addLineClass(breakpoint.lineNumber, 'background', breakpoint.className);
    this.codeMirror.scrollIntoView({line: breakpoint.lineNumber, ch: 0}, 100);
    this.breakpoint = breakpoint;
  }
  
  _hideCurrentBreakpoint() {
    if(null !== this.breakpoint) {
      this.codeMirror.removeLineClass(this.breakpoint.lineNumber, 'background', this.breakpoint.className);
      this.breakpoint = null;
    }
  }
  
  _mouseOver(event) {
    if(1 === ++this.showTooltip) {
      this.currentTarget = event.currentTarget;
      event.currentTarget.style = 'background-color:Lavender;cursor:pointer';
      this._addPopoverProperty(event);
      this._showTooltip();
    }
  }
  
  _mouseOut(event) {
    setTimeout(() => {
      if(0 === --this.showTooltip) {
        this._removePopoverProperty();
      }
    });
  }
  
  _mouseHandler(event) {
    if(event.cancelable) {
      event.cancelBubble = true;
    }
    if("mouseover" === event.type) {
      this._mouseOver(event);
    }
    else if("mouseout" === event.type) {
      this._mouseOut(event);
    }
  }
  
  _bindPopover() {
    $("span.cm-def").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-property").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-variable").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-variable-2").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-keyword").each((idx, element) => {
      CodeMirror.on(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._mouseHandler.bind(this));
    });
  }
  
  _unbindPopover() {
    $("span.cm-def").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-property").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-variable").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-variable-2").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
    $("span.cm-keyword").each((idx, element) => {
      CodeMirror.off(element, "mouseover", this._mouseHandler.bind(this));
      CodeMirror.off(element, "mouseout", this._mouseHandler.bind(this));
    });
  }
  
  _showDebugGutters(dbugger, name) {
    const gutterId = `breakpoints_${this.props.docKey}`;
    this.codeMirror.clearGutter(gutterId);
    dbugger.breakpoints.forEach((breakpoint) => {
      if(breakpoint.name === name) {
        this.codeMirror.setGutterMarker(breakpoint.lineNumber, gutterId, this._makeDebugMarker(breakpoint.lineNumber + 1, breakpoint.pauseOnBreak));
      }
    });
  }
  
  _showBuildGutters() {
    const gutterId = `builds_${this.props.docKey}`;
    this.codeMirror.clearGutter(gutterId);
    
    const build = this.props.build;
    build.errors.forEach((err) => {
      this.codeMirror.setGutterMarker(err.line - 1, gutterId, this._makeBuildMarker(err.msg));
    });
  }
  
  _addPopoverProperty(event) {
    const memberNameArray = [event.currentTarget.textContent];
    let previousSibling = event.currentTarget.previousSibling;
    while (null !== previousSibling) {
      if('.' !== previousSibling.textContent) {
        break;
      }
      previousSibling = previousSibling.previousSibling;
      if(null !== previousSibling) {
        memberNameArray.unshift(previousSibling.textContent);
      }
      previousSibling = previousSibling.previousSibling;
    }
      
    this.popoverDiv = document.createElement('div');
    this.reactDiv = this.popoverDiv.appendChild(document.createElement('div'));
    this.reactDiv.setAttribute('id', 'debug_popover');
    this.reactDiv.setAttribute('style', `left:${event.clientX - 10}px;top:${event.clientY}px;`);
    CodeMirror.on(this.reactDiv, "mouseover", this._tooltipMouseHandler.bind(this));
    CodeMirror.on(this.reactDiv, "mouseout", this._tooltipMouseHandler.bind(this));
    document.body.appendChild(this.popoverDiv);
    this._getTooltipValue(memberNameArray, event.currentTarget.className);
  }
  
  _removePopoverProperty() {
    if(null !== this.popoverDiv) {
      CodeMirror.off(this.reactDiv, "mouseover", this._tooltipMouseHandler.bind(this));
      CodeMirror.off(this.reactDiv, "mouseout", this._tooltipMouseHandler.bind(this));
      ReactDOM.unmountComponentAtNode(this.reactDiv);
      this.reactDiv = null;
      document.body.removeChild(this.popoverDiv);
      this.popoverDiv = null;
    }
    if(null !== this.currentTarget) {
      this.currentTarget.style = '';
      this.currentTarget = null;
    }
    this.props.onClearCurrentValue && this.props.onClearCurrentValue();
  }
  
  _getTooltipValue(memberNameArray, cmType) {
    if('cm-property' === cmType && 1 <= memberNameArray.length) {
      if('this' === memberNameArray[0]) {
        this.props.onGetCurrentValueByName && this.props.onGetCurrentValueByName(memberNameArray, 'this');
      } 
      else {
        this.props.onGetCurrentValueByName && this.props.onGetCurrentValueByName(memberNameArray, 'keyword');
      }
    }
    else if('cm-keyword' === cmType) {
      if(1 === memberNameArray.length) {
        if('this' === memberNameArray[0]) {
          this.props.onGetCurrentValueByName && this.props.onGetCurrentValueByName(memberNameArray, 'this');
        }
        else if('let' === memberNameArray[0]) {
          this.props.onGetKeyWord && this.props.onGetKeyWord(memberNameArray); 
        }
      }
    }
    else if('cm-def' === cmType ||'cm-variable-2' === cmType) {
      this.props.onGetCurrentValueByName && this.props.onGetCurrentValueByName(memberNameArray, 'local');
    }
  }
  
  _showPopup(dbugger) {
    if(undefined === this.reactDiv) {
      reactDiv = document.getElementById('debug_popover');
    }
    if(null !== this.reactDiv) {
      ReactDOM.render(<CodeMirrorPopover currentValue={dbugger.currentValue} objectValues={dbugger.objectValues}
        onGetMembers={(object, open) => {
          this.props.onGetMembers && this.props.onGetMembers(object, open);
        }}
        onClose={() => {
          this._removePopoverProperty();
          this.showTooltip = 0;
        }}
      />, this.reactDiv);
    }
  }
  
  _tooltipMouseHandler(event) {
    if(event.cancelable) {
      event.cancelBubble = true;
    }
    if("mouseover" === event.type) {
      ++this.showTooltip;
    }
    else if("mouseout" === event.type) {
      setTimeout(() => {
        if(0 === --this.showTooltip) {
          this._removePopoverProperty();
        }
      });
    }
  }
  
  _showTooltip() {
    const dbugger = this.props.dbugger;
    this._showPopup(dbugger);
    const element = $("#debug_popover");
    if(undefined !== element) {
      CodeMirror.on(element, "mouseover", this._tooltipMouseHandler.bind(this));
      CodeMirror.on(element, "mouseout", this._tooltipMouseHandler.bind(this));
    }    
  }
  
  _makeDebugMarker(lineNbr, pauseOnBreak) {
    const marker = document.createElement("div");
    const svg = marker.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'svg'));
    svg.classList.add('debug_marker');
    svg.setAttribute('width', 37);
    svg.setAttribute('height', 16);
    const group = svg.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    const arrow = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'polygon'));
    arrow.setAttribute('points', '0,1 28,1, 34,8.5 28,16 0,16');
    if(pauseOnBreak) {
      arrow.setAttribute('fill', 'CornflowerBlue');
    }
    else {
      arrow.setAttribute('fill', 'LightBlue');
    }
    const nbrSpan = marker.appendChild(document.createElement('span'));
    nbrSpan.classList.add('debug_marker');
    nbrSpan.classList.add('CodeMirror-linenumber');
    nbrSpan.classList.add('CodeMirror-gutter-elt');
    const nbr = nbrSpan.appendChild(document.createTextNode(lineNbr));
    return marker;
  }
  
  _makeBuildMarker(message) {
    const popoverDiv = document.createElement('div');
    ReactDOM.render(<CodeMirrorPopoverBuild message={message}
      onClose={() => {
        document.body.removeChild(popoverDiv);
      }}
    />, popoverDiv);
  
    const marker = document.createElement("div");
    const svg = marker.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'svg'));
    svg.classList.add('build_marker');
    svg.setAttribute('width', 13);
    svg.setAttribute('height', 13);
    svg.addEventListener("mouseover", (event) => {
      if(event.cancelable) {
        event.cancelBubble = true;
      }
      popoverDiv.setAttribute('style', `position:absolute;left:${event.clientX + 20}px;top:${event.clientY + 10}px;`);
      document.body.appendChild(popoverDiv);
    });
    const group = svg.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'g'));
    const circle = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'circle'));
    circle.classList.add('build_marker');
    circle.setAttribute('cx', 5);
    circle.setAttribute('cy', 8);
    circle.setAttribute('r', 5);
    const line1 = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    line1.classList.add('build_marker');
    line1.setAttribute('x1', 2);
    line1.setAttribute('x2', 8);
    line1.setAttribute('y1', 5);
    line1.setAttribute('y2', 11);
    const line2 = group.appendChild(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    line2.classList.add('build_marker');
    line2.setAttribute('x1', 2);
    line2.setAttribute('x2', 8);
    line2.setAttribute('y1', 11);
    line2.setAttribute('y2', 5);
    return marker;
  }
  
  onCodeChange(doc, change) {
    this.code = doc.getValue("\r\n");
    this.props.onChanged && this.props.onChanged(this.props.projectId, this.props.docKey, this.code);
  }
  
  onFocusChanged(focused) {
    if(this.focusTrigger) {
      this.props.onFocusChange && this.props.onFocusChange(focused, this.props.docKey);
    }
    else {
      this.focusTrigger = true;
    }
  }
  
  render () {
    return (
      <div className="CodeMirrorEditor">
        <textarea ref={(ref) => this._textarea = ref} />
      </div>
    );
  }
}
