
'use strict';

import CodeMirrorDebugVariable from './code-mirror-debug-variable';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class CodeMirrorPopover extends ReactComponentBase {
  constructor(props) {
    super(props);
  }

  shouldUpdate(nextProps, nextState) {
    return true;//!this.deepCompare(this.props.currentValue, nextProps.currentValue)
      //|| !this.deepCompare(this.props.objectValues, nextProps.objectValues)
  }
  
  renderTitle(id) {
    return (
      <div id={id}>
        <p id="debug_popover_title_title">
          {this.props.currentValue.name}
        </p>
        <button type="button" id="debug_popover_title_close" className="close" aria-label="Close"
          onClick={(e) => {
            this.props.onClose && this.props.onClose();
          }}
        >
          ×
        </button>
      </div>
    );
  }
  
  renderValue() {
    return (
      <div id="debug_popover_tooltip">
        {this.renderTitle('debug_popover_title_value')}
        <div id="debug_popover_content">
          <CodeMirrorDebugVariable x="CodeMirrorPopover" name={this.props.currentValue.name} value={this.props.currentValue.value} objectValues={this.props.objectValues} 
            onGetMembers={(object, open) => {
              this.props.onGetMembers && this.props.onGetMembers(object, open);
            }}  
          />
        </div>
      </div>
    );
  }
  
  renderFunction() {
    return (
      <div id="debug_popover_tooltip">
        {this.renderTitle('debug_popover_title_function')}
      </div>
    );
  }
  
  render() {
    if(undefined === this.props.currentValue.value) {
      return null;
    }
    else {
      switch(this.props.currentValue.value.type) {
        case 'function':
          return this.renderFunction();
        default:
          return this.renderValue();
      }
    }
  }
}
