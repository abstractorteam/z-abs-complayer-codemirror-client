
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class CodeMirrorDebugVariable extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareObjectValues(this.props, nextProps)
  }
  
  renderScopeValueMembersInnerExtra(renderedMembers, extraMembers, depth) {
    if(undefined !== extraMembers) {
      let length = extraMembers.find((member) => {
        return 'length' === member.name;
      }).value.value;
      for(let i = 0; i < length; ++i) {
        let member = extraMembers.find((member) => {
          return i.toString() === member.name;
        });
        renderedMembers.push(this.renderScopeValue(member, i, depth));
      }
    }
  }
  
  renderScopeValueMembersInner(renderedMembers, members, depth) {
    members.forEach((member, index) => {
      renderedMembers.push(this.renderScopeValue(member, index, depth));
    });
  }
  
  renderScopeValueMembers(object, depth, div) {
    if(null !== object.value) {
      let objectValue = this.props.objectValues.get(object.objectId);
      let renderedMembers = [];
      let divClass = 'debug_scope_closed_object';
      if(undefined !== objectValue && objectValue.open) {
        this.renderScopeValueMembersInnerExtra(renderedMembers, objectValue.extraMembers, depth);
        this.renderScopeValueMembersInner(renderedMembers, objectValue.members, depth);
        divClass = 'debug_scope_open_object';
      }
      return (
        <div ref={(svg) => { div.push(svg); }} className={divClass}>
          <ul className="nav nav-pills nav-stacked debug_scope">
            {renderedMembers}
          </ul>
        </div>
      );
    }
  }
  
  renderScopeImage(object, isNull) {
    if(isNull) {
      return null;
    }
    let svgClass = 'debug_scope_closed_object';
    if("object" === object.type) {
      let objectValue = this.props.objectValues.get(object.objectId);
      if(undefined !== objectValue && objectValue.open) {
        svgClass = 'debug_scope_open_object';
      }
    }
    else if("local" === object.type || "local" === object.type) {
      if(this.props.open) {
        svgClass = 'debug_scope_open_object';
      }
    }
    let svgRef = null;
    return (
      <svg ref={(svg) => { svgRef = svg; }} className={svgClass} width="9" height="9"
        onClick={(e) => {
          e.preventDefault();
          if("object" === object.type) {
            this.props.onGetMembers && this.props.onGetMembers(object, !svgRef.classList.contains('debug_scope_open_object'));
          }
          else if("local" === object.type || "local" === object.type) {
            this.props.onOpen && this.props.onOpen(!this.props.open);
          }
        }
      }>
        <g>
          <polygon points="1,1 1,7 7,4" stroke="Grey" strokeWidth="1" fill="white" />
        </g>
      </svg>
    );
  }

  renderScopeValueUndefined() {
    return (
      <span className="debug_def debug_scope">
        undefined
      </span>
    );
  }
  
  renderScopeValueNull() {
    return (
      <span className="debug_atom debug_scope">
        null
      </span>
    );
  }
  
  renderScopeValueObject(object, isNull, isUndefined) {
    if(isUndefined) {
      return this.renderScopeValueUndefined();
    }
    else if(isNull) {
      return this.renderScopeValueNull();
    }
    else {
      return (
        <span className="debug_variable debug_scope">
          {object.value.className}
        </span>
      );
    }
  }
    
  renderScopeValue(object, index, depth) {
    let key = index + '-' + depth;
    if(undefined !== object.value) {
      if("local" === object.value.type) {
        let locals = object.value.object.map((local, index) => {
          return this.renderScopeValue(local, index, depth + 1);
        });
        let divClass = this.props.open ? 'debug_scope_open_object' : 'debug_scope_closed_object';
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
              }}
            >
              {this.renderScopeImage(object.value, 0 === locals.length)}
              <p className="debug_scope">
                {object.name}: 
              </p>
            </a>
            <div className={divClass}>
              <ul className="nav nav-pills nav-stacked debug_scope">
                {locals}
              </ul>
            </div>
          </li>
        );
      }
      else if('string' === object.value.type) {
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
              }}
            >
              <p className="debug_scope">
                {object.name}: 
              </p>
              <span className="debug_string debug_scope">
                "{object.value.value}"
              </span>
              <p className="debug_scope_type">
                [string]
              </p>
            </a>
          </li>
        );
      }
      else if('number' === object.value.type) {
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
            }}>
              <p className="debug_scope">
                {object.name}: 
              </p>
              <span className="debug_number debug_scope">
                {object.value.value}
              </span>
              <p className="debug_scope_type">
                [number]
              </p>
            </a>
          </li>
        );
      }
      else if('boolean' === object.value.type) {
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
            }}>
              <p className="debug_scope">
                {object.name}: 
              </p>
              <span className="debug_atom debug_scope">
                {object.value.value ? 'true' : 'false'}
              </span>
              <p className="debug_scope_type">
                [boolean]
              </p>
            </a>
          </li>
        );
      }
      else if('object' === object.value.type) {
        let type = '[object]';
        let isUndefined = undefined === object.value;
        let isNull = !isUndefined && null === object.value.value;
        if(!isUndefined && undefined !== object.value.subtype) {
          if('generator' === object.value.subtype) {
            type = `[${object.value.subtype}]`;
          }
          else if(!isNull) {
            type = `[${object.value.description}]`;
          }
        }
        let divArray = [];
        let members = this.renderScopeValueMembers(object.value, depth + 1, divArray);
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
            }}>
              {this.renderScopeImage(object.value, isNull)}
              <p className="debug_scope">
                {object.name}: 
              </p>
              {this.renderScopeValueObject(object, isNull, isUndefined)}
              <p className="debug_scope_type">
                {type}
              </p>
            </a>
            {members}
          </li>
        );
      }
      else if('undefined' === object.value.type) {
        return (
          <li key={key} role="presentation" className="debug_scope">
            <a className="debug_scope" href="#" 
              onClick={(e) => {
                e.preventDefault();
                //this.dispatch(TestCaseStore, new ActionTestCaseDebugLevel(index));
            }}>
              <p className="debug_scope">
                {object.name}: 
              </p>
              {this.renderScopeValueUndefined()}
            </a>
          </li>
        );
      }
      else {
        return null;
      }
    }
  }

  render() {
    return (
      <ul className="nav nav-pills nav-stacked debug_scope">
        {
          this.renderScopeValue({
            name: this.props.name,
            value: this.props.value
          }, 0, 0)
        }
      </ul>
    );
  }
}
